<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Autor
Route::post('/Author', 'AuthorController@store')->name('Author.store');
Route::post('/AuthorLIn', 'AuthorController@show')->name('Author.show');

//Blog

Route::get('/Blog', 'BlogController@index')->name('Blog.index');
Route::post('/Blog', 'BlogController@store')->name('Blog.store');
Route::get('/Blog/{Blog}', 'BlogController@show')->name('Blog.show');
Route::put('/Blog/{Blog}', 'BlogController@update')->name('Blog.update');

//Comment

Route::post('/Comment', 'CommentController@store')->name('Comment.store');
Route::get('/Comment', 'CommentController@index')->name('Comment.index');

