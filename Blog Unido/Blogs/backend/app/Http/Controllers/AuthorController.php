<?php

namespace App\Http\Controllers;

use App\Author;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
        $author = new Author();
        $author->Name = $request->Name;
        $author->Password = $request->Password;
        $author->Email = $request->Email;
        $author->save();
        return response()->json(['message'=> 'Autor creado', 
        'Author' => $author]); */
        
        $validatedData = $request->validate([
            'Name' => 'required',
            'Password' => 'required',
            'Email' => 'required', 
        ]);
        $Author =Author::create([
            'Name' => $validatedData['Name'],
            'Password' => $validatedData['Password'],
            'Email' => $validatedData['Email']
          ]);
  
          return response()->json('Author created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        request()->validate([
            'Password' => 'required',
            'Email' => 'required',
            ]);
        $email=$request->get('Email');
        $consulta = Author::where('Email', '=', $email)->first();
        if($consulta==null){
            return -1;
        }else{
            
            $feed= json_decode($consulta,TRUE);
            $var=$feed["Password"];
            $id = $feed["id"];

            $pasConfir = $request->get('Password');
            if($var==$pasConfir){
                return $id;
            }
            else{
                return -1;
            }
        }
        

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function edit(Author $author)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Author $author)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function destroy(Author $author)
    {
        //
    }
}
