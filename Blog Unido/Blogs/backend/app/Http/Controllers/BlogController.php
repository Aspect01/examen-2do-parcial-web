<?php

namespace App\Http\Controllers;

use App\Author;
use App\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Blog = Blog::all();
        return response()->json($Blog);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData=$request->validate([
            'id_author' => 'required',
            'Theme' => 'required',
            'Content' => 'required', 
            'Date' => 'required', 
        ]);
        $Blog = Blog::create([
            'id_author' => $validatedData['id_author'],
            'Theme' => $validatedData['Theme'],
            'Content' => $validatedData['Content'],
            'Date' => $validatedData['Date']
          ]);
  
          return response()->json('Project created!');

        //$Blog = Blog::create($request->all());
        //return response()->json(['message'=> 'Blog Creado', 
        //'Blog' => $Blog]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$Blog = Blog::join('authors', 'authors.id', '=', 'blogs.id_author')
        //->where('blogs.id', '=', $id);
        $Blog = Blog::find($id);
        //$idaut = $Blog->validate([
        //    'id_author' => 'required'
        //]);
        //$autor = Author::find($idaut['id_author']);
        return $Blog;
        //return response()->json([
        //    'Author' => $autor,
        //    'Blog' => $Blog
        //]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $Blog = Blog::where('id', '=', $id)->first();

        $Blog->update($request->all());
        return response()->json([
            'message' => 'Blog Actualizado!',
            'Blog' => $Blog
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        //
    }
}
