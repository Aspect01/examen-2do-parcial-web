import React, { Component } from "react";
import axios from 'axios';

export default class NameAuth extends Component{
    constructor(props) {
        super(props);
    
        // State
        this.state = {
            Name: ''
            
        };
      }

      componentDidMount() {
    

          axios.get("http://localhost:8000/api/Author/" + this.props.match.params.id)
          .then(resp => {
            this.setState({
              Name: resp.data.Name
            });
          })
          
          .catch((error) => {
            console.log(error);
          })

      }
      render(){
          return(
            <h3>{this.state.Name}</h3>
          );

      }



}