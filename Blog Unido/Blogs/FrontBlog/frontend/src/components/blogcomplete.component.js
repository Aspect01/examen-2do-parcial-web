import React, { Component } from "react";
import axios from 'axios';

import ListComment from "./listComment.component";
import CreateComment from "./createComment.component";

export default class blogcomplete extends Component{
    constructor(props) {
        super(props);
    
        // State
        this.state = {
            id_author: '',
            Theme: '',
            Content: '',
            Dat: '',
            Name: ''
            
        };
      }

      componentDidMount() {
    
        axios.get("http://localhost:8000/api/Blog/" + this.props.match.params.id)
          .then(res => {
            this.setState({
              id_author: res.data.id_author,
                Theme: res.data.Theme,
                Content: res.data.Content,
                Dat: res.data.Date,
                Name: res.data.Name
            });
            console.log(this.state);
          })
          .catch((error) => {
            console.log(error);
          })

          

      }


      render(){
        console.log(this.state);
          return(<div>
            
            <h1>{this.state.Theme}</h1>
            <h3>{this.state.Dat}</h3>
            <h3>Usuario: {this.state.id_author}</h3>
            <textarea name="textarea" rows="10" cols="50" value={this.state.Content}></textarea>
            <a href="javascript:history.go(-1)">Atrás</a>
            <CreateComment />
            <br></br>
            <br></br>
            <ListComment/>
          </div>
          );

      }
}