import React, { Component } from "react";
import axios from 'axios'
import Swal from 'sweetalert2';

export default class CreateComment extends Component{
    constructor(props) {
        super(props)
    
        // Setting up functions
        this.onChangeContent = this.onChangeContent.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    
        // Setting up state   Inicializar autor y blog
        this.state = {
          id_author: 4,
          id_blog: 2,
          Content: ''
        }
      }

    
      onChangeContent(e) {
        this.setState({Content: e.target.value})
      }
    
     

      onSubmit(e) {
        e.preventDefault()
         const Comment = {
          id_author: this.state.id_author,
          id_blog: this.state.id_blog,
          Content: this.state.Content,
        }
        axios.post('http://localhost:8000/api/Comment/', Comment)
          .then(res => console.log(res.data));
        Swal.fire(
      'Buen Trabajo!',
      'Comentario añadido',
      'Exito'
    )
    
        this.setState({Content: ''})
    }
    

      render() {
        return (

                <div className='form-group'>
                    <form onSubmit={this.onSubmit}>
                        <div class="form-group">
                            <label>Comentarios</label>

                            <textarea id="comment" class="form-control" onChange={this.onChangeContent}></textarea>

                        </div>

                        <button className='btn btn-primary'>Comentar</button>

                    </form>

          </div>
          
        );
      }

}