import React, { Component } from "react";
import { Link } from 'react-router-dom';
import axios from 'axios';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';



export default class blogfrontList extends Component {

  constructor(props) {
    super(props)
    this.state = {
      blogfronts: []
    };
  }

  componentDidMount() {
    axios.get('http://localhost:8000/api/Blog/')  
      .then(res => {
        this.setState({
          blogfronts: res.data
        });
      })
      .catch((error) => {
        console.log(error);
      })
  }




  render() {
    return (<div className="table-wrapper">
      <h1>Lista de Blogs</h1>  <h2>Usuario Nro: {this.props.match.params.id}</h2>
      
      <Link className="Create-link" to={"/blogcreate/" + this.props.match.params.id}>
        <Button size="sm" variant="info">Crear Nuevo Tema</Button>
      </Link>
      <form action="/">
        <p><button class="btn-danger" type="submit">Cerrar Sesion</button></p>
      </form>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Autor</th>
            <th>Tema</th>
            <th>Fecha</th>            
          </tr>
        </thead>
        <tbody>
            {this.state.blogfronts.map((res, i) => {
              return (
                <tr key={res.id}>
                  <td>Usuario: {res.id_author}</td>
                  <td>
                    <Link className="blogcomplete-link" to={"/blogcomplete/" + res.id}>
                      {res.Theme}
                    </Link>
                  </td>
                  <td>
                    {res.Date}
                  </td>
                </tr>
              );
            })}
        </tbody>
      </Table>
      
    </div>);
  }
}