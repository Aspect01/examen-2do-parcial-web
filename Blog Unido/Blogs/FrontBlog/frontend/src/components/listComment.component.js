import React, { Component } from "react";
import axios from 'axios';
import Table from 'react-bootstrap/Table';

export default class ListComment extends Component {

  constructor(props) {
    super(props)
    this.state = {
      commentfronts: []
    };
  }

  componentDidMount() {
    axios.get('http://localhost:8000/api/Comment/')  
      .then(res => {
        this.setState({
          commentfronts: res.data
        });
      })
      .catch((error) => {
        console.log(error);
      })
  }

  render() {
    return (<div className="table-wrapper">
      <h3>Comentarios</h3>
      
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Autor</th>
            <th>Comentarios</th>
            <th>Fecha</th>            
          </tr>
        </thead>
        <tbody>
            {this.state.commentfronts.map((res, i) => {
              return (
                <tr key={res.id}>
                  <td>Usuario: {res.id_author}</td>
                  <td>{res.Content}</td>
                  <td>{res.created_at.substr(0,10)}</td>
                </tr>
              );
            })}
        </tbody>
      </Table>
    </div>);
  }
}