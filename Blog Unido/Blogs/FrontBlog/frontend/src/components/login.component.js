import React, { Component } from "react";
import axios from 'axios';
import Swal from 'sweetalert2'

export default class Login extends Component {

    constructor(props) {
        super(props);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.Respuesta='';
        this.state = {
            Password: '',
            Email: '',
            Resp: ''
          }

    }
    onChangePassword(e) {
        this.setState({Password: e.target.value})
      }
    
      onChangeEmail(e) {
        this.setState({Email: e.target.value})
      }


      onSubmit(e) {
        e.preventDefault()
         const Author = {
          Password: this.state.Password,
          Email: this.state.Email
        };
        axios.post('http://localhost:8000/api/AuthorLIn/', Author)
          .then(res => {
            this.setState({
              Resp: res.data 
            });
          });
      }


    render() {
      if(this.state.Resp !== ''){
        console.log(this.state);
        if(this.state.Resp>0){
          Swal.fire("Aceptado");
          window.location.href = '/blogfront-listing/'+this.state.Resp;
        }else{
          Swal.fire("Vuelva a Intentarlo");
          this.setState({Password: '', Email: '', Resp: ''});
        }
      }
        return (
            
            <form onSubmit={this.onSubmit}>
                <h3>Ingresar</h3>

                <div className="form-group">
                    <label>Direccion de Correo</label>
                    <input type="email" className="form-control" placeholder="Enter email" onChange={this.onChangeEmail} />
                </div>

                <div className="form-group">
                    <label>Contraseña</label>
                    <input type="password" className="form-control" placeholder="Enter password" onChange={this.onChangePassword}  />
                </div>


                <button type="submit" className="btn btn-primary btn-block">Aceptar</button>
                <p className="forgot-password text-right">
                    
                    <a href="/sign-up">Registrarse</a>
                </p>
            </form>
        );
    }
}