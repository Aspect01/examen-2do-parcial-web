import React, { Component } from "react";
import axios from 'axios'
import Swal from 'sweetalert2';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button';

export default class createblogfront extends Component{
    constructor(props) {
        super(props)
        // Setting up functions
        this.onChangeTheme = this.onChangeTheme.bind(this);
        this.onChangeContent = this.onChangeContent.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    
        // Setting up state
        this.state = {
          id_author: props.match.params.id,
          Theme: '',
          Content: '',
          Date: '',
          errors:[]
        }
      }

    
      onChangeTheme(e) {
        this.setState({Theme: e.target.value})
      }
    
      onChangeContent(e) {
        this.setState({Content: e.target.value})
      }
    
     

      onSubmit(e) {
        var f = new Date();
        e.preventDefault()
         const Blog = {
          id_author: this.state.id_author,
          Theme: this.state.Theme,
          Content: this.state.Content,
          Date: f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate()
        }
        axios.post('http://localhost:8000/api/Blog/', Blog)
          .then(res => console.log(res.data));
        Swal.fire(
      'Buen Trabajo!',
      'Tema añadido',
      'Exito'
    )
    
        this.setState({Theme: '', Content: '', Date: '' })
    }
    

      render() {
        return (
        
            <div className='container py-4'>
            <div className='row justify-content-center'>
 
            <form onSubmit={this.onSubmit}>


                <div className='form-group'>
                    <label htmlFor='Theme'>Titulo Tema</label><br></br>
                    <input
                    id='Theme'
                    type='text'
                    name='Theme'
                    value={this.state.Theme}
                    onChange={this.onChangeTheme}
                    />
                </div>
                <div className='form-group'>
                    <label htmlFor='description'>Contenido</label><br></br>
                    <textarea
                    id='Content'
                    name='Content'
                    rows='5'
                    value={this.state.Content}
                    onChange={this.onChangeContent}
                    />
                </div>
                <button className='btn btn-primary'>Create</button>
                </form>
            </div>
            <br></br>

            <Link className="Create-link" to={"/blogfront-listing/" + this.props.match.params.id}>
              <Button size="sm" variant="danger">Regresar</Button>
            </Link>
          </div>   
        );
      }
}