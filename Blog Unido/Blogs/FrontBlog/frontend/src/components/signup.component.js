import React, { Component } from "react";
import axios from 'axios'
import Swal from 'sweetalert2';

export default class SignUp extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            Name: '',
            Password: '',
            Email: ''
          }
    }

    onChangeName(e) {
        this.setState({Name: e.target.value})
      }
    
      onChangePassword(e) {
        this.setState({Password: e.target.value})
      }
    
      onChangeEmail(e) {
        this.setState({Email: e.target.value})
      }


      onSubmit(e) {
        e.preventDefault()
         const Author = {
          Name: this.state.Name,
          Password: this.state.Password,
          Email: this.state.Email
        };
        axios.post('http://localhost:8000/api/Author/', Author)
          .then(res => console.log(res.data));
        // console.log(`Personal successfully created!`);
        // console.log(`Name: ${this.state.name}`);
        // console.log(`LastName: ${this.state.LastName}`);
        // console.log(`Cell: ${this.state.Cell}`);
        Swal.fire(
      'Buen Trabajo!',
      'Cuenta Creada ',
      'Exitosamente'
    )
    
        this.setState({Name: '', Password: '', Email: ''})
      }


    render() {
        // eslint-disable-next-line
        return (
            <form onSubmit={this.onSubmit}>
                <h3>Registro</h3>

                <div className="form-group">
                    <label>Nombre</label>
                    <input type="text" className="form-control" placeholder="Nombre Completo" onChange={this.onChangeName} />
                </div>

                <div className="form-group">
                    <label>Direccion de Correo</label>
                    <input type="email" className="form-control" placeholder="Ingrese su Email" onChange={this.onChangeEmail} />
                </div>

                <div className="form-group">
                    <label>Contraseña</label>
                    <input type="password" className="form-control" placeholder="Ingrese su password" onChange={this.onChangePassword} />
                </div>

                <button type="submit" className="btn btn-primary btn-block">Registrarse</button>
                <p className="forgot-password text-right">
                    
                    Ya Registrado? <a href="/">Ingrese</a>
                </p>
            </form>
        );
    }
}