
import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";

import Login from "./components/login.component";
import SignUp from "./components/signup.component";
import blogfrontList from "./components/blogfront-listing.component";
import blogcomplete from "./components/blogcomplete.component";
import createblogfront from "./components/create-blogfront.component";
import ListComment from "./components/listComment.component";
import CreateComment from "./components/createComment.component";
import NameAuth from "./components/nameAuth.component";

function App() {
  return (<Router>
    <div className="App">
      
      <br></br>
      <div className="auth-wrapper">
        <div className="auth-inner">
          
          <Switch>
            <Route exact path='/' component={Login} />
            <Route path="/sign-in" component={Login} />
            <Route path="/sign-up" component={SignUp} />
            <Route path="/blogfront-listing/:id" component={blogfrontList} />
            <Route path="/blogcomplete/:id" component={blogcomplete} />
            <Route path="/blogcreate/:id" component={createblogfront} />
            <Route path="/listComment" component={ListComment} />
            <Route path="/createComment" component={CreateComment} />
            <Route path="/nameAuth/:id" component={NameAuth} />

          </Switch>
        </div>
      </div>
      
    </div></Router>
  );
}

export default App;
//
