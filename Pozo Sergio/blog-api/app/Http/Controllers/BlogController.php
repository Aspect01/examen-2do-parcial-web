<?php

namespace App\Http\Controllers;

use App\blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog = blog::all();
        return response()->json($blog);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_user' => 'required',
            'theme' => 'required',
            'content' => 'required',
            'date' => 'required'
        ]);
        $blog = blog::create($request->all());
        return response()->json(['message'=> 'blog created',
        'blog' => $blog]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(blog $blog)
    {
        return $blog;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, blog $blog)
    {
        $request->validate([
            'id_user' => 'required',
            'theme' => 'required',
            'content' => 'required',
            'date' => 'required'

        ]);
        $blog->id_user = $request->id_user();
        $blog->theme = $request->theme();
        $blog->content = $request->content();
        $blog->date = $request->date();
        $blog->save();
        //$personal=Personal::findOrFail($id);
        //$personal->update($request->all());

        return response()->json([
            'message' => 'blog updated!',
            'blog' => $blog
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $blog = blog::where('id', '=', $id)->first();

        $blog->update($request->all());
        return response()->json([
            'message' => 'blog updated!',
            'blog' => $blog
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(blog $blog)
    {
        //
    }
}
