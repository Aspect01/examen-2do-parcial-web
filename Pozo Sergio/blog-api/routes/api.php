<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/blog', 'blogController@index')->name('blog.index');

Route::post('/blog', 'blogController@store')->name('blog.store');

Route::get('/blog/{blog}', 'blogController@show')->name('blog.show');

Route::put('/blog/{blog}', 'blogController@update')->name('blog.update');

//Route::delete('/blog/{blog}', 'blogController@destory')->name('blog.destroy');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
