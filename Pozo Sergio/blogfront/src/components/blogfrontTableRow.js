import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import axios from 'axios';

export default class blogfrontTableRow extends Component {
    constructor(props) {
        super(props);
        this.deleteblogfront = this.deleteblogfront.bind(this);
    }

    deleteblogfront() {
        axios.delete('http://localhost:8000/api/blogfront/' + this.props.obj.id)
            .then((res) => {
                console.log('blog removed deleted!')
            }).catch((error) => {
                console.log(error)
            })
        
    }
    render() {
        return (
            <tr>
                <td>{this.props.obj.id_user}</td>
                <td>{this.props.obj.theme}</td>
                
                <td>{this.props.obj.date}</td>
                
                <td>
                    <Link className="edit-link" to={"/edit-blogfront/" + this.props.obj.id}>
                       <Button size="sm" variant="info">editarblog</Button>
                    </Link>
                    <Button onClick={this.deleteblogfront} size="sm" variant="danger">blogelim</Button>
                </td>
            </tr>
        );
    }
}

