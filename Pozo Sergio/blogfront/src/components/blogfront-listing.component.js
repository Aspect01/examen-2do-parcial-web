import React, { Component } from "react";
import axios from 'axios';
import Table from 'react-bootstrap/Table';
import blogfrontTableRow from './blogfrontTableRow';


export default class blogfrontList extends Component {

  constructor(props) {
    super(props)
    this.state = {
      blogfronts: []
    };
  }

  componentDidMount() {
    axios.get('http://localhost:8000/api/blogfront/')
      .then(res => {
        this.setState({
          blogfronts: res.data
        });
      })
      .catch((error) => {
        console.log(error);
      })
  }

  DataTable() {
    return this.state.blogfronts.map((res, i) => {
      return <blogfrontTableRow obj={res} key={i} />;
    });
  }


  render() {
    return (<div className="table-wrapper">
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>id_user</th>
            <th>theme</th>
            
            <th>date</th>
            
          </tr>
        </thead>
        <tbody>
          {this.DataTable()}
        </tbody>
      </Table>
    </div>);
  }
}