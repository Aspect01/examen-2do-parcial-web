import React, { Component } from "react";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button';
import axios from 'axios';

export default class Editblogfront extends Component {

  constructor(props) {
    super(props)

    this.onChangeblogfrontid_user = this.onChangeblogfrontid_user.bind(this);
    this.onChangeblogfronttheme = this.onChangeblogfronttheme.bind(this);
    this.onChangeblogfrontcontent = this.onChangeblogfrontcontent.bind(this);
    this.onChangeblogfrontdate = this.onChangeblogfrontdate.bind(this);
    //this.onChangePersonalAge = this.onChangePersonalAge.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    // State
    this.state = {
        id_user: '',
        theme: '',
        content: '',
        date: '',
        //Age: ''
      }
    }

  componentDidMount() {
    
    axios.get('http://localhost:8000/api/blogfront/' + this.props.match.params.id)
      .then(res => {
        this.setState({
          id_user: res.data.id_user,
          theme: res.data.theme,
          content: res.data.content,
          date: res.data.date,
          //Age: res.data.Age
        });
      })
      .catch((error) => {
        console.log(error);
      })
  }

  onChangeblogfrontid_user(e) {
    this.setState({ id_user: e.target.value })
  }

  onChangeblogfronttheme(e) {
    this.setState({ theme: e.target.value })
  }

  onChangeblogfrontcontent(e) {
    this.setState({ content: e.target.value })
  }

  onChangeblogfrontdate(e) {
    this.setState({date: e.target.value})
  }

  

  onSubmit(e) {
    //e.preventDefault()
    
    const blogfrontObject = {
      id_user: this.state.id_user,
      theme: this.state.theme,
      content: this.state.content,
      date: this.state.date,
      //Age: this.state.Age
    };

    axios.put('http://localhost:8000/api/blogfront/' + this.props.match.params.id,blogfrontObject)
      .then((res) => {
        console.log(res.data)
        console.log('blog successfully updated')
      }).catch((error) => {
        console.log(error)
      })

    // Redirect to Personal List 
    this.props.history.push('/blogfront-listing')
  }


  render() {
    return (<div className="form-wrapper">
      <Form onSubmit={this.onSubmit}>
        <Form.Group controlId="id_user">
          <Form.Label>Nombre</Form.Label>
          <Form.Control type="text" value={this.state.id_user} onChange={this.onChangeblogfrontid_user} />
        </Form.Group>

        <Form.Group controlId="theme">
          <Form.Label>Apellido</Form.Label>
          <Form.Control type="text" value={this.state.theme} onChange={this.onChangeblogfronttheme} />
        </Form.Group>

        <Form.Group controlId="content">
          <Form.Label>Celular</Form.Label>
          <Form.Control type="text" value={this.state.content} onChange={this.onChangeblogfrontcontent} />
        </Form.Group>

        <Form.Group controlId="date">
          <Form.Label>Email</Form.Label>
          <Form.Control type="date" value={this.state.date} onChange={this.onChangeblogfrontdate} />
        </Form.Group>

        <Button variant="danger" size="lg" block="block" type="submit">
          Actualizar Personal
        </Button>
      </Form>
    </div>);
  }
}