import React, { Component } from "react";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import axios from 'axios'
import blogfrontList from './blogfront-listing.component';
import Swal from 'sweetalert2';


export default class Createblogfront extends Component {
      constructor(props) {
    super(props)

    // Setting up functions
    this.onChangeblogfrontid_user = this.onChangeblogfrontid_user.bind(this);
    this.onChangeblogfronttheme = this.onChangeblogfronttheme.bind(this);
    this.onChangeblogfrontcontent = this.onChangeblogfrontcontent.bind(this);
    this.onChangeblogfrontdate = this.onChangeblogfrontdate.bind(this);
    //this.onChangePersonalAge = this.onChangePersonalAge.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    // Setting up state
    this.state = {
      id_user: '',
      theme: '',
      content: '',
      date: '',
      //Age: ''
    }
  }

  onChangeblogfrontid_user(e) {
    this.setState({name: e.target.value})
  }

  onChangeblogfronttheme(e) {
    this.setState({LastName: e.target.value})
  }

  onChangeblogfrontcontent(e) {
    this.setState({Cell: e.target.value})
  }

  onChangeblogfrontdate(e) {
    this.setState({Email: e.target.value})
  }

  //onChangePersonalAge(e) {
    //this.setState({Age: e.target.value})
  //}

  onSubmit(e) {
    e.preventDefault()
     const blogfront = {
      id_user: this.state.id_user,
      theme: this.state.theme,
      content: this.state.content,
      date: this.state.date,
      //Age: this.state.Age
    };
    axios.post('http://localhost:8000/api/blogfront/', blogfront)
      .then(res => console.log(res.data));
    // console.log(`Personal successfully created!`);
    // console.log(`Name: ${this.state.name}`);
    // console.log(`LastName: ${this.state.LastName}`);
    // console.log(`Cell: ${this.state.Cell}`);
    Swal.fire(
  'Good job!',
  'blogdis Added Successfully',
  'success'
)

    this.setState({id_user: '', theme: '', content: '', date: ''})
  }

  render() {
    return (<div className="form-wrapper">
      <Form onSubmit={this.onSubmit}>
        <Row> 
            <Col>
             <Form.Group controlId="id_user">
                <Form.Label>id_user</Form.Label>
                <Form.Control type="text" value={this.state.id_user} onChange={this.onChangeblogfrontid_user}/>
             </Form.Group>
            
            </Col>
            
            <Col>
             <Form.Group controlId="theme">
                <Form.Label>tema</Form.Label>
                        <Form.Control type="text" value={this.state.theme} onChange={this.onChangeblogfronttheme}/>
             </Form.Group>
            </Col> 

            <Col>
                <Form.Group controlId="content">
                    <Form.Label>contenido</Form.Label>
                        <Form.Control type="text" value={this.state.content} onChange={this.onChangeblogfrontcontent}/>
                </Form.Group>
            </Col> 

            <Col>
             <Form.Group controlId="date">
                <Form.Label>date</Form.Label>
                        <Form.Control type="date" value={this.state.date} onChange={this.onChangeblogfrontdate}/>
             </Form.Group>
            </Col>            
        </Row>
            

        

       
        <Button variant="primary" size="lg" block="block" type="submit">
          Agregar blog
        </Button>
      </Form>
      <br></br>
      <br></br>

      <blogfrontList> </blogfrontList>
    </div>);
  }
}
