import React from 'react';
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import 'bootstrap/dist/css/bootstrap.css';
//
import './App.css';


import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Editblogfront from "./components/edit-blogfront.component";
import blogfrontList from "./components/blogfront-listing.component";
import Createblogfront from "./components/create-blogfront.component";


function App() {
  return (<Router>
    <div className="App">
      <header className="App-header">
        <Navbar bg="success" variant="success">
          <Container>

            <Nav className="justify-content-end">
              <Nav>
                <Link to={"/create-blogfront"} className="nav-link">
                  blog C
                </Link>
              </Nav>
            </Nav>

          </Container>
        </Navbar>
      </header>

      <Container>
        <Row>
          <Col md={12}>
            <div className="wrapper">
              <Switch>
                <Route exact path='/' component={Createblogfront} />
                <Route path="/create-blogfront" component={Createblogfront} />
                <Route path="/edit-blogfront/:id" component={Editblogfront} />
                <Route path="/blogfront-listing" component={blogfrontList} />
              </Switch>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  </Router>);
}

export default App;
