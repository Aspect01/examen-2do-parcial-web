import React, { Component } from "react";
import axios from 'axios';
import Table from 'react-bootstrap/Table';
import comentariosTableRow from './comentariosTableRow';


export default class comentarioList extends Component {

  constructor(props) {
    super(props)
    this.state = {
      comentarios: []
    };
  }

  componentDidMount() {
    axios.get('http://localhost:8000/api/comentarios/Prueba')  //a cambiar despues
      .then(res => {
        this.setState({
          comentarios: res.data
        });
      })
      .catch((error) => {
        console.log(error);
      })
  }

  DataTable() {
    return this.state.comentarios.map((res, i) => {
      return <comentariosTableRow obj={res} key={i} />;
    });
  }


  render() {
    return (<div className="table-wrapper">
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Usuario</th>
            <th>Comentario</th>
            <th>Fecha</th>
          </tr>
        </thead>
        <tbody>
          {this.DataTable()}
        </tbody>
      </Table>
    </div>);
  }
}