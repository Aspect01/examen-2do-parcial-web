import React, { Component } from "react";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import axios from 'axios'
import comentarioList from './comentario-listing.component';
import Swal from 'sweetalert2';


export default class Createcomentario extends Component {
      constructor(props) {
    super(props)

    // Setting up functions
    this.onChangecomentarioComment = this.onChangecomentarioComment.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    // Setting up state
    this.state = {
      Comment: ''
    }
  }

  onChangecomentarioComment(e) {
    this.setState({Comment: e.target.value})
  }


  onSubmit(e) {
    var d = new Date();
    var fecha = d.getFullYear()+"/"+d.getMonth()+"/"+d.getDate()+" "+d.getHours()+":"+d.getMinutes+":"+d.getSeconds;
    e.preventDefault()
     const comentario = {
      User: "Rodrigo",
      Comment: this.state.Comment,
      Theme: "Prueba",
      Moment: fecha
    };
    axios.post('http://localhost:8000/api/comentarios/', comentario)
      .then(res => console.log(res.data));
    // console.log(`comentario successfully created!`);
    // console.log(`Comment: ${this.state.Comment}`);
    // console.log(`LastComment: ${this.state.LastComment}`);
    // console.log(`Cell: ${this.state.Cell}`);
    Swal.fire(
  'Good job!',
  'Comment Added Successfully',
  'success'
)

    this.setState({Comment: ''})
  }

  render() {
    return (<div classComment="form-wrapper">
      <Form onSubmit={this.onSubmit}>
        <Form.Group controlId="Comment">
            <Form.Label>Comentario</Form.Label>
            <Form.Control as="textarea" type="textarea" value={this.state.Comment} onChange={this.onChangecomentarioComment}/>
        </Form.Group>
            
        <Button variant="primary" size="lg" block="block" type="submit">
          Enviar
        </Button>
      </Form>
      <br></br>
      <br></br>

      <comentarioList> </comentarioList>
    </div>);
  }
}
