import React, { Component } from 'react';


export default class comentariosTableRow extends Component {
    constructor(props) {
        super(props);
        this.deletecomentarios = this.deletecomentarios.bind(this);
    }

    render() {
        return (
            <tr>
                <td>{this.props.obj.User}</td>
                <td>{this.props.obj.Comment}</td>
                <td>{this.props.obj.Moment}</td>
            </tr>
        );
    }
}