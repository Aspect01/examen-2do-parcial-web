import React from 'react';
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
//import Row from "react-bootstrap/Row";
//import Col from "react-bootstrap/Col";
import 'bootstrap/dist/css/bootstrap.css';
//
import './App.css';

// eslint-disable-next-line
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom"; 

import comentarioList from "./components/comentario-listing.component";
import Createcomentario from "./components/create-comentario.component";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Navbar bg="success" variant="success">
          <Container>

            <Nav className="justify-content-end">
              <Nav>
                <Link to={"/create-comentario"} className="nav-link">
                  Dejar Comentario
                </Link>
              </Nav>
            </Nav>

          </Container>
        </Navbar>
      </header>

      <Container>
        
        <Router>
          <Switch>
              <Route exact path='/' component={Createcomentario} />
              <Route path="/create-comentario" component={Createcomentario} />
              <Route path="/comentario-listing" component={comentarioList} />
            </Switch>
        </Router>
      </Container>



    </div>
  );
}

export default App;
