<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

#Route::get('/comentarios', 'ComentariosController@index')->name('comentarios.index');

Route::post('/comentarios', 'ComentariosController@store')->name('comentarios.store');
# para subir comentario
Route::get('/comentarios/{comentarios}', 'ComentariosController@show')->name('comentarios.show');
# para ver comentarios de acuerdo a tema

